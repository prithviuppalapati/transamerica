importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

if (workbox) {
  workbox.precaching.precacheAndRoute([
  {
    "url": "pwa-starter/favicon.ico",
    "revision": "1fbdf735a0dd3e8321c5e0828a45a4d5"
  },
  {
    "url": "pwa-starter/index.html",
    "revision": "9d1375ba8507499617bfb64138c0943e"
  },
  {
    "url": "pwa-starter/main-es2015.a743a52b168378fbbd93.js",
    "revision": "e2d65e9ca9d507c5c0abbbaa4ac7f8fa"
  },
  {
    "url": "pwa-starter/main-es5.a743a52b168378fbbd93.js",
    "revision": "e99e3c987f8721acdc587efefa05cb18"
  },
  {
    "url": "pwa-starter/polyfills-es2015.2987770fde9daa1d8a2e.js",
    "revision": "2316636eea5b286a66699c3bdfcc10e6"
  },
  {
    "url": "pwa-starter/polyfills-es5.6696c533341b95a3d617.js",
    "revision": "44fdb532a19e811b016fc191fcdbfded"
  },
  {
    "url": "pwa-starter/runtime-es2015.edb2fcf2778e7bf1d426.js",
    "revision": "1244d3f2f28ecc6619157927aca95200"
  },
  {
    "url": "pwa-starter/runtime-es5.edb2fcf2778e7bf1d426.js",
    "revision": "1244d3f2f28ecc6619157927aca95200"
  }
]);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}