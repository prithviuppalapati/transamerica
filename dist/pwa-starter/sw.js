importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

if (workbox) {
  workbox.precaching.precacheAndRoute([
  {
    "url": "assets/add.png",
    "revision": "52c04f4fde76a445611632ba852f5d4f"
  },
  {
    "url": "assets/back-icon.png",
    "revision": "4f8f6eff660d50f4d8961aac06e00a4c"
  },
  {
    "url": "assets/benefit-tab.png",
    "revision": "b05e88e434f30202d4273012a2b160fc"
  },
  {
    "url": "assets/benefits-icon.png",
    "revision": "cca721e36b9203e99347d74e242495d2"
  },
  {
    "url": "assets/earphone-icon.png",
    "revision": "cdba4ebccbdd850987d4ddd88f1341ef"
  },
  {
    "url": "assets/group-3-copy.svg",
    "revision": "9aeb448ff0774442203791a65d0e3fda"
  },
  {
    "url": "assets/group-5.png",
    "revision": "1956e8919c7250fb8c080cb83494d284"
  },
  {
    "url": "assets/home-desktop.jpg",
    "revision": "dcfd4a48dc1cda7dafa91ad94d0dee62"
  },
  {
    "url": "assets/home.jpg",
    "revision": "3f6c882bc007ba56cffe43484f98e327"
  },
  {
    "url": "assets/icon-1125x2436.jpg",
    "revision": "dc1525fa036892f4709e8de787270be8"
  },
  {
    "url": "assets/icon-1136x640.jpg",
    "revision": "337fa5d6bd00b30da2ff388bb8527e5e"
  },
  {
    "url": "assets/icon-1242x2208.jpg",
    "revision": "b3bbc641baa55600f984936f4ebb1894"
  },
  {
    "url": "assets/icon-1242x2688.jpg",
    "revision": "e4a5489941de46aa9500f2c608da28ba"
  },
  {
    "url": "assets/icon-128x128.png",
    "revision": "e8cb81e5826b22ac18c9005c624430e5"
  },
  {
    "url": "assets/icon-144x144.png",
    "revision": "4f23a541e5afb43462f40c9002d4c8cf"
  },
  {
    "url": "assets/icon-152x152.png",
    "revision": "098dbfe721a3a7bf4f6f15a0fe5f6803"
  },
  {
    "url": "assets/icon-1536x2048.jpg",
    "revision": "43d3cf3bc3fada2994c662c5e765815a"
  },
  {
    "url": "assets/icon-1668x2224.jpg",
    "revision": "001499153c41804e9aaf80e0a5059618"
  },
  {
    "url": "assets/icon-1668x2388.jpg",
    "revision": "b217a08ef69990f9e056c9544dc8e42a"
  },
  {
    "url": "assets/icon-1792x828.jpg",
    "revision": "cd135e3a8cdbebc6267837d03ecc91fd"
  },
  {
    "url": "assets/icon-180x180.png",
    "revision": "0f77d9ec50ba683b39551eaa0c37d4fe"
  },
  {
    "url": "assets/icon-192x192.png",
    "revision": "a6fc92cd9a826656bba3679e3ad296cc"
  },
  {
    "url": "assets/icon-2048x1536.jpg",
    "revision": "40598939aa4459b6529bacb5bd3ddd2b"
  },
  {
    "url": "assets/icon-2048x2732.jpg",
    "revision": "026cf7d9f01883945ff8f479b84fe29c"
  },
  {
    "url": "assets/icon-2208x1242.jpg",
    "revision": "974ced107ae3dcd0a1ec6b9b2a02594e"
  },
  {
    "url": "assets/icon-2224x1668.jpg",
    "revision": "2a7d398c2c47134fc87cfba95a391c47"
  },
  {
    "url": "assets/icon-2388x1668.jpg",
    "revision": "46a17a5212f178641c396d1b1f3cc53d"
  },
  {
    "url": "assets/icon-2436x1125.jpg",
    "revision": "631e56483c51fa44394ba7b4c14b5ba1"
  },
  {
    "url": "assets/icon-2688x1242.jpg",
    "revision": "d726e4334f419f82c34265cb4d51d843"
  },
  {
    "url": "assets/icon-2732x2048.jpg",
    "revision": "3cb910f2f5d5fc48620d2ad203578f3f"
  },
  {
    "url": "assets/icon-384x384.png",
    "revision": "f0a98d582ec381e42d75e386653493a9"
  },
  {
    "url": "assets/icon-512x512.png",
    "revision": "0ea8e731275943f7f27be46108d8df0c"
  },
  {
    "url": "assets/icon-640x1136.jpg",
    "revision": "d38ef21c2aa03f6aa3af338fec04e229"
  },
  {
    "url": "assets/icon-72x72.png",
    "revision": "7442af2a94120587fc81c5a60d037dae"
  },
  {
    "url": "assets/icon-750x1334.jpg",
    "revision": "bee7f2e04e8f9fb0bbbbdad92ee1c0ba"
  },
  {
    "url": "assets/icon-828x1792.jpg",
    "revision": "7e2e405c24738d06288d83d8ec440ed0"
  },
  {
    "url": "assets/icon-96x96.png",
    "revision": "d248017c7e9aa25ffcc38120e76708b0"
  },
  {
    "url": "assets/trans-logo.png",
    "revision": "cb7a6321b65473b5aa3b77b9e6ddf1b6"
  },
  {
    "url": "favicon.png",
    "revision": "39431e5a4c457d68b8bbea92538141d5"
  },
  {
    "url": "home-desktop.dcfd4a48dc1cda7dafa9.jpg",
    "revision": "dcfd4a48dc1cda7dafa91ad94d0dee62"
  },
  {
    "url": "home.3f6c882bc007ba56cffe.jpg",
    "revision": "3f6c882bc007ba56cffe43484f98e327"
  },
  {
    "url": "index.html",
    "revision": "bd634328c5b7ef2b08c2bd393e4381ec"
  },
  {
    "url": "main-es2015.a61df51b571285a9bc90.js",
    "revision": "8365642fcd15bf4a81a203b20f21c737"
  },
  {
    "url": "main-es5.a61df51b571285a9bc90.js",
    "revision": "cc7d164b13f378422dbe7368b6af8e89"
  },
  {
    "url": "polyfills-es2015.5b10b8fd823b6392f1fd.js",
    "revision": "5a76543bff280f58485274121a50b679"
  },
  {
    "url": "polyfills-es5.3e8196928d184a6e5319.js",
    "revision": "d67fc379b7de9009d4883639e6aca4eb"
  },
  {
    "url": "runtime-es2015.c5fa8325f89fc516600b.js",
    "revision": "1244d3f2f28ecc6619157927aca95200"
  },
  {
    "url": "runtime-es5.c5fa8325f89fc516600b.js",
    "revision": "1244d3f2f28ecc6619157927aca95200"
  },
  {
    "url": "styles.3700f74286778da3011e.css",
    "revision": "f4969621832d0a2c6556df5c821841c5"
  }
]);
  workbox.routing.registerRoute(
    /\.js$/,
    new workbox.strategies.NetworkFirst()
  );
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

workbox.routing.registerRoute(
  new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
  workbox.strategies.cacheFirst({
    cacheName: 'google-fonts',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      }),
    ],
  }),
);

self.addEventListener('push', function(event) {
  var content=event.data.text();

  var text;
    text = JSON.parse(content).title;
    event.waitUntil(self.registration.showNotification(text, {
        body: 'tap to view',
        data: '',
        icon: "https://pwa-angular-8.herokuapp.com/assets/icon-72x72.png"
      }));
});

self.addEventListener('notificationclick', function(event) {
  console.log('Notification click: tag', event.notification.tag);
  event.notification.close();
  var url = "https://pwa-angular-8.herokuapp.com/#/Confirmation?policyId=G0926TS53";
  event.waitUntil(
    clients.matchAll({
      type: 'window'
    })
    .then(function(windowClients) {
      console.log('WindowClients', windowClients);
      for (var i = 0; i < windowClients.length; i++) {
        var client = windowClients[i];
        console.log('WindowClient', client);
        if (client.url === url && 'focus' in client) {
          return client.focus();
        }
      }
      if (clients.openWindow) {
        return clients.openWindow(url);
      }
    })
  );
});
