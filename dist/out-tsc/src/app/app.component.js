import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { fromEvent, of, merge } from "rxjs";
let AppComponent = class AppComponent {
    constructor(commonService, _snackBar, router) {
        this.commonService = commonService;
        this._snackBar = _snackBar;
        this.router = router;
        this.title = 'pwa-starter';
        this.showButton = false;
        this.readyForPush = false;
        this.page = '';
        window.deviceOnline = true;
        this.router.events.subscribe((val) => {
            if (val.url === '/') {
                this.page = 'login';
            }
            else if (val.url !== '/' && val.url) {
                this.page = '';
                if (val.url.includes('benefits') && window.Notification && (window.Notification.permission === 'default' || window.Notification.permission === 'granted')) {
                    if (window.Notification.permission === 'default') {
                        this._snackBar.open('Allow Notification?', 'Okay!', {
                            duration: 10000,
                        }).onAction().subscribe(() => this.initiatePush());
                    }
                    else {
                        this.initiatePush();
                    }
                }
            }
        });
    }
    ngOnInit() {
        this.online$ = merge(of(navigator.onLine), fromEvent(window, "online"), fromEvent(window, "offline"));
        this.online$.subscribe((x) => {
            let self = this;
            if (x === true || x.type == "online") {
                window.isDeviceOnline = true;
                if (localStorage.getItem('offlineExists') === 'yes') {
                    localStorage.setItem('offlineExists', 'no');
                    this.commonService.notifyUser({ user: localStorage.getItem('name') }).subscribe((res) => {
                        console.log(res);
                    });
                }
            }
            else if (x.type == "offline") {
                window.isDeviceOnline = false;
            }
        });
    }
    onbeforeinstallprompt(e) {
        console.log(e);
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault();
        // Stash the event so it can be triggered later.
        this.deferredPrompt = e;
        this.showButton = true;
        this._snackBar.open('Install App', 'Okay!', {
            duration: 4000,
        }).onAction().subscribe(() => this.addToHomeScreen());
    }
    addToHomeScreen() {
        // hide our user interface that shows our A2HS button
        this.showButton = false;
        // Show the prompt
        this.deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        this.deferredPrompt.userChoice
            .then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
            }
            else {
                console.log('User dismissed the A2HS prompt');
            }
            this.deferredPrompt = null;
        });
    }
    onswRegisteredCustom(e) {
        this.readyForPush = true;
    }
    urlBase64ToUint8Array(base64String) {
        const padding = "=".repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, "+")
            .replace(/_/g, "/");
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);
        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }
    initiatePush() {
        let self = this;
        if (typeof swRegistration !== "undefined" && swRegistration && swRegistration.pushManager) {
            console.log(swRegistration);
            let convertedVapidKey = self.urlBase64ToUint8Array("BD15-ky6pTNm3dDaWU9hpTY2Y02C7NTy5VH79EuRI7vhStTKODXezFm06qRVOwHUfZm7qmEljOKwlnoA18RM_ZI");
            swRegistration.pushManager
                .subscribe({
                userVisibleOnly: true,
                applicationServerKey: convertedVapidKey
            })
                .then(function (sub) {
                if (sub) {
                    let mySub = JSON.parse(JSON.stringify(sub));
                    mySub.user = localStorage.getItem('name');
                    self.commonService.sendPushSubscription(mySub).subscribe((i) => {
                        console.log(i);
                    });
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
    }
};
tslib_1.__decorate([
    HostListener('window:beforeinstallprompt', ['$event'])
], AppComponent.prototype, "onbeforeinstallprompt", null);
tslib_1.__decorate([
    HostListener('window:swRegisteredCustom', ['$event'])
], AppComponent.prototype, "onswRegisteredCustom", null);
AppComponent = tslib_1.__decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map