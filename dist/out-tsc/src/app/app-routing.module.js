import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClaimsComponent } from './Business/claims/claims.component';
import { BenefitsComponent } from './Business/benefits/benefits.component';
import { BenefitDetailsComponent } from './Business/benefit-details/benefit-details.component';
import { ConfirmationComponent } from './Common/Components/confirmation/confirmation.component';
import { LoginComponent } from './Common/Components/login/login.component';
const routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'claims',
        component: ClaimsComponent
    },
    {
        path: 'benefits',
        component: BenefitsComponent
    },
    {
        path: 'benefitDetails',
        component: BenefitDetailsComponent
    },
    {
        path: 'Confirmation',
        component: ConfirmationComponent
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map