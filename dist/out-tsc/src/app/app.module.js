import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClaimsComponent } from './Business/claims/claims.component';
import { BenefitsComponent } from './Business/benefits/benefits.component';
import { BenefitDetailsComponent } from './Business/benefit-details/benefit-details.component';
import { ConfirmationComponent } from './Common/Components/confirmation/confirmation.component';
import { CommonService } from './Common/Services/common.service';
import { LoginComponent } from './Common/Components/login/login.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableComponent } from './Common/Components/table/table.component';
import { HeaderComponent } from './Common/Components/header/header.component';
import { DatePipe } from '@angular/common';
import { FooterComponent } from './Common/Components/footer/footer.component';
import { PreviewDialogComponent } from './Business/claims/preview-dialog/preview-dialog.component';
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            ClaimsComponent,
            BenefitsComponent,
            BenefitDetailsComponent,
            ConfirmationComponent,
            LoginComponent,
            TableComponent,
            FooterComponent,
            HeaderComponent,
            PreviewDialogComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            MaterialModule,
            FlexLayoutModule,
            BrowserAnimationsModule,
            HttpClientModule,
            ReactiveFormsModule,
            FormsModule
        ],
        providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, CommonService, DatePipe],
        bootstrap: [AppComponent],
        entryComponents: [PreviewDialogComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map