import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Constants } from '../../Common/Constants/constants';
let BenefitsComponent = class BenefitsComponent {
    constructor(commonService, router) {
        this.commonService = commonService;
        this.router = router;
        this.benefitColumns = Constants.BENEFITS_TABLE_COLUMNS;
    }
    ngOnInit() {
        this.fetchBenefitsData();
    }
    fetchBenefitsData() {
        this.commonService.getPolicyData().subscribe((policyList) => {
            this.claimedPolicyList = policyList;
            policyList.forEach(list => {
                list['insuredName'] = list['insured']['firstName'] + ' ' + list['insured']['lastName'];
            });
            this.activePolicyList = policyList.filter(function (list) {
                return list.policyClaimed === false;
            });
            this.claimedPolicyList = policyList.filter(function (list) {
                return list.policyClaimed === true;
            });
        });
    }
    getDate(dateAndTime) {
        return dateAndTime.split('T', 1);
    }
    navigateToPolicyDetails(policyId) {
        const params = {
            queryParams: {
                policyId: policyId
            }
        };
        this.router.navigate(['/benefitDetails'], params);
    }
};
BenefitsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-benefits',
        templateUrl: './benefits.component.html',
        styleUrls: ['./benefits.component.scss']
    })
], BenefitsComponent);
export { BenefitsComponent };
//# sourceMappingURL=benefits.component.js.map