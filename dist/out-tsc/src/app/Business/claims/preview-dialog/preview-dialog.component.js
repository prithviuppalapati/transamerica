import * as tslib_1 from "tslib";
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
let PreviewDialogComponent = class PreviewDialogComponent {
    constructor(data, dialogRef, dialog) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.dialog = dialog;
        this.previewObject = {
            'Gender': '',
            'FullName': '',
            'Relationship': '',
            'dob': '',
            'attachDoc': '',
            'doa': '',
            'dos': '',
            'autoText': '',
            'servicePlace': '',
            'servicePerformed': '',
            'provider': ''
        };
    }
    ngOnInit() {
        this.generateForm();
    }
    closeDialog() {
        this.dialogRef.close();
    }
    generateForm() {
        this.data.forEach(ele => {
            if (ele.questionLabel === 'Gender') {
                ele.options.forEach(innEle => {
                    if (innEle.value) {
                        this.previewObject.Gender = innEle.label;
                    }
                });
            }
            else if (ele.questionLabel === 'Patient Name ?') {
                let FullName = '';
                ele.options.forEach((innEle, index) => {
                    FullName = FullName + ' ' + innEle.value;
                });
                if (FullName) {
                    this.previewObject.FullName = FullName;
                }
                else {
                    this.previewObject.FullName = 'NA';
                }
            }
            else if (ele.questionLabel === 'Relationship with Employee ?') {
                ele.options.forEach((innEle, index) => {
                    if (innEle.value) {
                        this.previewObject.Relationship = innEle.label;
                    }
                });
            }
            else if (ele.questionLabel === 'Date of Birth ?') {
                ele.options.forEach((innEle, index) => {
                    if (innEle.value) {
                        this.previewObject.dob = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'Upload Document ?') {
                ele.options.forEach((innEle, index) => {
                    if (index === 0 && innEle.value) {
                        this.previewObject.attachDoc = innEle.value[0][0].name;
                    }
                });
            }
            else if (ele.questionLabel === 'Date of Accident ?') {
                ele.options.forEach((innEle, index) => {
                    if (innEle.value) {
                        this.previewObject.doa = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'Date of Service ?') {
                ele.options.forEach((innEle) => {
                    if (innEle.value) {
                        this.previewObject.dos = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'If auto accident the patient was ?') {
                ele.options.forEach((innEle) => {
                    if (innEle.value) {
                        this.previewObject.autoText = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'Service Performed ?') {
                ele.options.forEach((innEle) => {
                    if (innEle.value) {
                        this.previewObject.servicePerformed = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'Place of Service ?') {
                ele.options.forEach((innEle) => {
                    if (innEle.value) {
                        this.previewObject.servicePlace = innEle.value;
                    }
                });
            }
            else if (ele.questionLabel === 'Pay Provider ?') {
                ele.options.forEach((innEle, index) => {
                    if (innEle.value) {
                        this.previewObject.provider = innEle.label;
                    }
                });
            }
        });
    }
};
PreviewDialogComponent = tslib_1.__decorate([
    Component({
        selector: 'app-preview-dialog',
        templateUrl: './preview-dialog.component.html',
        styleUrls: ['./preview-dialog.component.scss'],
        encapsulation: ViewEncapsulation.None
    }),
    tslib_1.__param(0, Inject(MAT_DIALOG_DATA))
], PreviewDialogComponent);
export { PreviewDialogComponent };
//# sourceMappingURL=preview-dialog.component.js.map