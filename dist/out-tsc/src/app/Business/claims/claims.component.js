import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Mock } from '../../Common/Mock/Mock';
import { MatDialogConfig } from '@angular/material/dialog';
import { PreviewDialogComponent } from './preview-dialog/preview-dialog.component';
let ClaimsComponent = class ClaimsComponent {
    constructor(route, _snackBar, router, dialog, commonService) {
        this.route = route;
        this._snackBar = _snackBar;
        this.router = router;
        this.dialog = dialog;
        this.commonService = commonService;
        this.questionSetsPatientInformation = [];
        this.questionSetsPolicyInformation = [];
        this.questionSet = [];
        this.queIndex = 0;
        this.progressiveCount = 5;
        this.isLast = false;
        this.isNextDiabled = true;
        this.attachments = [];
        this.photos = [];
    }
    ngOnInit() {
        this.questionSetsPatientInformation = JSON.parse(JSON.stringify(Mock.questionSetsPatientInformation));
        this.questionSetsPolicyInformation = JSON.parse(JSON.stringify(Mock.questionSetsPolicyInformation));
        this.questionSet = this.questionSet.concat(this.questionSetsPatientInformation, this.questionSetsPolicyInformation);
    }
    selectOption(selectOption, options) {
        options.forEach(element => {
            element.value = false;
        });
        selectOption.value = true;
        this.checkDisabled();
    }
    goBack() {
        const params = {
            queryParams: {
                policyId: this.route.snapshot.queryParams.policyId
            }
        };
        this.router.navigate(['/benefitDetails'], params);
    }
    goBacktoBenfits() {
        this.router.navigate(['/benefits']);
    }
    getPreview() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = false;
        dialogConfig.width = '350px';
        dialogConfig.disableClose = true;
        dialogConfig.data = this.questionSet;
        dialogConfig.panelClass = 'full-width-dialog';
        const dialogRef = this.dialog.open(PreviewDialogComponent, dialogConfig);
    }
    checkDisabled() {
        const progCount = 100 / this.questionSet.length;
        this.questionSet.forEach((question, index) => {
            question.options.forEach(data => {
                if (data.value) {
                    this.progressiveCount = progCount * (index + 1);
                }
                if (data.value && index === this.queIndex) {
                    this.isNextDiabled = false;
                }
            });
        });
    }
    getNextQuestions() {
        this.queIndex++;
        this.isNextDiabled = true;
        this.checkDisabled();
        if ((this.questionSet.length - 1) == this.queIndex) {
            this.isLast = true;
        }
    }
    getLocation(value) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                const latlng = new google.maps.LatLng(pos.lat, pos.lng);
                const geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'latLng': latlng
                }, function (results, status) {
                    if (status ==
                        google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            value = results[1].formatted_address;
                        }
                        else {
                            alert('No results found');
                        }
                    }
                    else {
                        value = 'Geocoder failed due to: ' + status;
                    }
                });
            }, function () {
            });
            value = value;
        }
    }
    save() {
    }
    onSubmit() {
        if (!window.isDeviceOnline) {
            this._snackBar.open('No Internet Connection. We will Submit your claim, when your device is Online', 'Okay!', {
                duration: 4000,
            }).onAction().subscribe(() => {
                localStorage.setItem('offlineExists', 'yes');
                this.router.navigate(['']);
            });
        }
        else {
            const params = {
                queryParams: {
                    policyId: this.route.snapshot.queryParams.policyId
                }
            };
            this.commonService.notifyUser({ user: localStorage.getItem('name') }).subscribe((res) => {
                console.log(res);
            });
            this.router.navigate(['Confirmation'], params);
        }
    }
    getPreviousQuestions() {
        if (this.queIndex > 0) {
            this.queIndex--;
            this.isLast = false;
            this.isNextDiabled = false;
        }
    }
    onBrowseClick() {
        this.fileUpload.nativeElement.click();
    }
    onFileSelected(event, queIndex) {
        if (event.target.files && event.target.files.length > 0) {
            const files = event.target.files;
            this.attachments.push(files);
            this.questionSet[queIndex].options[0].value = this.attachments;
        }
    }
    getPhoto() {
        this.photoUpload.nativeElement.click();
    }
    upload(queIndex) {
        return new Promise((resolve, reject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            const filePicker = document.querySelector('#photoUploadId');
            if (!filePicker || !filePicker.files
                || filePicker.files.length <= 0) {
                reject('No file selected.');
                return;
            }
            const myFile = filePicker.files[0];
            this.questionSet[queIndex].options[1].value = this.photos.push(myFile);
            resolve();
        }));
    }
    deletePhoto(index) {
        this.photos.splice(index, 1);
    }
    deleteAttach(index) {
        this.attachments.splice(index, 1);
    }
};
tslib_1.__decorate([
    ViewChild('fileUpload', { static: false })
], ClaimsComponent.prototype, "fileUpload", void 0);
tslib_1.__decorate([
    ViewChild('photoUpload', { static: false })
], ClaimsComponent.prototype, "photoUpload", void 0);
ClaimsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-claims',
        templateUrl: './claims.component.html',
        styleUrls: ['./claims.component.scss'],
        encapsulation: ViewEncapsulation.None
    })
], ClaimsComponent);
export { ClaimsComponent };
//# sourceMappingURL=claims.component.js.map