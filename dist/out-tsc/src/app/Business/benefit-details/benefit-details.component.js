import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let BenefitDetailsComponent = class BenefitDetailsComponent {
    constructor(route, commonService, router) {
        this.route = route;
        this.commonService = commonService;
        this.router = router;
    }
    ngOnInit() {
        this.fetchBenefitDetails();
    }
    fetchBenefitDetails() {
        const param = this.route.snapshot.queryParams.policyId;
        this.commonService.getIndividualPolicyData(param).subscribe((policyData) => {
            this.policyData = policyData[0];
        });
    }
    getDate(dateAndTime) {
        return dateAndTime.split('T', 1);
    }
    navigateToClaim() {
        const params = {
            queryParams: {
                policyId: this.route.snapshot.queryParams.policyId
            }
        };
        this.router.navigate(['/claims'], params);
    }
    goBacktoBenfits() {
        this.router.navigate(['/benefits']);
    }
};
BenefitDetailsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-benefit-details',
        templateUrl: './benefit-details.component.html',
        styleUrls: ['./benefit-details.component.scss']
    })
], BenefitDetailsComponent);
export { BenefitDetailsComponent };
//# sourceMappingURL=benefit-details.component.js.map