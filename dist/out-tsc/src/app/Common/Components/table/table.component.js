import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ColumnType } from '../../enums/column-type';
let TableComponent = class TableComponent {
    constructor(datePipe) {
        this.datePipe = datePipe;
        this.getDetails = new EventEmitter();
        this.columnTypes = ColumnType;
    }
    ngOnInit() {
        this.dataSource = new MatTableDataSource(this.dataSet);
    }
    ngAfterViewInit() {
        this.dataSource = new MatTableDataSource(this.dataSet);
    }
    ngOnChanges(changes) {
        if (changes.dataSet) {
            this.dataSource = new MatTableDataSource(changes.dataSet.currentValue);
        }
    }
    getDisplayColumnsList() {
        if (this.columns) {
            return this.columns.map(column => column.name);
        }
    }
    navigateToDetails(element) {
        this.getDetails.emit(element);
    }
};
tslib_1.__decorate([
    Input()
], TableComponent.prototype, "dataSet", void 0);
tslib_1.__decorate([
    Input()
], TableComponent.prototype, "isStickyHeader", void 0);
tslib_1.__decorate([
    Input()
], TableComponent.prototype, "columns", void 0);
tslib_1.__decorate([
    Output()
], TableComponent.prototype, "getDetails", void 0);
TableComponent = tslib_1.__decorate([
    Component({
        selector: 'app-table',
        templateUrl: './table.component.html',
        styleUrls: ['./table.component.scss']
    })
], TableComponent);
export { TableComponent };
//# sourceMappingURL=table.component.js.map