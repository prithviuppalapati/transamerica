import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation } from '@angular/core';
import { Validators } from '@angular/forms';
let LoginComponent = class LoginComponent {
    constructor(router, formBuilder, commonService) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.commonService = commonService;
    }
    ngOnInit() {
        this.loginFormBuilder();
    }
    loginFormBuilder() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    navigateToBenefits() {
        localStorage.setItem('name', this.loginForm.value.username);
        this.commonService.loginUser({ 'user': this.loginForm.value.username }).subscribe((i) => {
            console.log(i);
        });
        this.router.navigate(['/benefits']);
    }
};
LoginComponent = tslib_1.__decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.scss'],
        encapsulation: ViewEncapsulation.None
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map