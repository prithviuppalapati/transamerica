import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation, Input } from '@angular/core';
let HeaderComponent = class HeaderComponent {
    constructor() { }
    ngOnInit() {
        this.date = new Date().toString().substring(4, 21).replace('2020', '2020,');
    }
    upload() {
        return new Promise((resolve, reject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            const filePicker = document.querySelector('input');
            if (!filePicker || !filePicker.files
                || filePicker.files.length <= 0) {
                reject('No file selected.');
                return;
            }
            const myFile = filePicker.files[0];
            console.log(myFile);
            resolve();
        }));
    }
};
tslib_1.__decorate([
    Input()
], HeaderComponent.prototype, "isHidden", void 0);
HeaderComponent = tslib_1.__decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.scss'],
        encapsulation: ViewEncapsulation.None
    })
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map