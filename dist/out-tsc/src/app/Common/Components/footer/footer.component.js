import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation } from '@angular/core';
let FooterComponent = class FooterComponent {
    constructor() {
        this.showFooter = false;
    }
    ngOnInit() {
    }
};
FooterComponent = tslib_1.__decorate([
    Component({
        selector: 'app-footer',
        templateUrl: './footer.component.html',
        styleUrls: ['./footer.component.scss'],
        encapsulation: ViewEncapsulation.None
    })
], FooterComponent);
export { FooterComponent };
//# sourceMappingURL=footer.component.js.map