export class Mock {
}
Mock.questionSetsPatientInformation = [
    {
        'questionLabel': 'Providers Address ?',
        'options': [
            {
                'label': 'Type here',
                'value': ''
            }
        ],
        'type': 'textarea',
        'buttonText': 'Add Current Location'
    },
    {
        'questionLabel': 'Gender',
        'options': [
            {
                'label': 'Male',
                'value': false
            },
            {
                'label': 'Female',
                'value': false
            }
        ],
        'type': 'radio',
        'checkedValue': ''
    },
    {
        'questionLabel': 'Patient Name ?',
        'options': [
            {
                'label': 'First Name',
                'value': ''
            },
            {
                'label': 'Last Name',
                'value': ''
            }
        ],
        'type': 'input'
    },
    {
        'questionLabel': 'Relationship with Employee ?',
        'options': [
            {
                'label': 'Self',
                'value': false
            },
            {
                'label': 'Father',
                'value': false
            },
            {
                'label': 'Mother',
                'value': false
            },
            {
                'label': 'Other',
                'value': false
            }
        ],
        'type': 'radio'
    },
    {
        'questionLabel': 'Date of Birth ?',
        'options': [
            {
                'label': 'Select Date',
                'value': ''
            }
        ],
        'type': 'date'
    }
];
Mock.questionSetsPolicyInformation = [
    {
        'questionLabel': 'Upload Document ?',
        'options': [
            {
                'label': 'Attach Doc',
                'value': ''
            },
            {
                'label': 'Take Photo',
                'value': ''
            }
        ],
        'type': 'button'
    },
    {
        'questionLabel': 'Date of Accident ?',
        'options': [
            {
                'label': 'Select Date',
                'value': ''
            }
        ],
        'type': 'date'
    },
    {
        'questionLabel': 'Date of Service ?',
        'options': [
            {
                'label': 'Select Date',
                'value': ''
            }
        ],
        'type': 'date'
    },
    {
        'questionLabel': 'If auto accident the patient was ?',
        'options': [
            {
                'label': 'Type here',
                'value': ''
            }
        ],
        'type': 'textarea'
    },
    {
        'questionLabel': 'Place of Service ?',
        'options': [
            {
                'label': 'Type here',
                'value': ''
            }
        ],
        'type': 'textarea'
    },
    {
        'questionLabel': 'Service Performed ?',
        'options': [
            {
                'label': 'Type here',
                'value': ''
            }
        ],
        'type': 'textarea'
    },
    {
        'questionLabel': 'Pay Provider ?',
        'options': [
            {
                'label': 'Pay benefit to provider',
                'value': false
            },
            {
                'label': 'Pay benefit to me',
                'value': false
            }
        ],
        'type': 'radio'
    }
];
Mock.uploadDocument = [];
//# sourceMappingURL=Mock.js.map