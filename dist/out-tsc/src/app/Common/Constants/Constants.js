export class Constants {
}
Constants.BENEFITS_TABLE_COLUMNS = [
    {
        displayName: 'InsuredName',
        name: 'insuredName',
        type: 'string',
    },
    {
        displayName: 'Product',
        name: 'policyType',
        type: 'string'
    },
    {
        displayName: 'Contract ID',
        name: 'policyId',
        type: 'string'
    },
    {
        displayName: 'Iussue Date',
        name: 'policyExpiryDate',
        type: 'date'
    },
    {
        displayName: 'Status',
        name: 'policyStatus',
        type: 'string',
        hyperLink: true
    },
    {
        displayName: 'Action',
        name: 'Action',
        type: 'menu'
    }
];
//# sourceMappingURL=constants.js.map