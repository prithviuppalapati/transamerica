export var ColumnType;
(function (ColumnType) {
    ColumnType["NUMBER"] = "number";
    ColumnType["STRING"] = "string";
    ColumnType["CURRENCY"] = "currency";
    ColumnType["DATE"] = "date";
    ColumnType["DATE_TIME"] = "date_time";
    ColumnType["TIME"] = "time";
    ColumnType["MENU"] = "menu";
})(ColumnType || (ColumnType = {}));
//# sourceMappingURL=column-type.js.map