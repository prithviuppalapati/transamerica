import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
let CommonService = class CommonService {
    constructor(http) {
        this.http = http;
    }
    getPolicyData() {
        return this.http.get(`${environment.endpointIP}/getPolicyDetails`);
    }
    getIndividualPolicyData(policyId) {
        return this.http.get(`${environment.endpointIP}/getPolicyDetails/${policyId}`);
    }
    sendPushSubscription(body) {
        return this.http.post(`${environment.endpointIP}/subscription`, body);
    }
    loginUser(body) {
        return this.http.post(`${environment.endpointIP}/createuser`, body);
    }
    notifyUser(user) {
        return this.http.post(`${environment.endpointIP}/notify`, user);
    }
};
CommonService = tslib_1.__decorate([
    Injectable()
], CommonService);
export { CommonService };
//# sourceMappingURL=common.service.js.map