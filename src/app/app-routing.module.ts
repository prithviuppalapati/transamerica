import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClaimsComponent } from './Business/claims/claims.component';
import { BenefitsComponent } from './Business/benefits/benefits.component';
import { BenefitDetailsComponent } from './Business/benefit-details/benefit-details.component';
import { ConfirmationComponent } from './Common/Components/confirmation/confirmation.component';
import { LoginComponent } from './Common/Components/login/login.component';
import { ContactUsComponent } from './ContactUs/contact-us.component';
const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'claims',
    component: ClaimsComponent
  },
  {
    path: 'benefits',
    component: BenefitsComponent
  },
  {
    path: 'benefitDetails',
    component: BenefitDetailsComponent
  },
  {
    path: 'Confirmation',
    component: ConfirmationComponent
  },
  {
    path: 'contactUs',
    component: ContactUsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
