import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../Common/Services/common.service';
import { Router, NavigationExtras } from '@angular/router';
import { Constants } from '../../Common/Constants/constants';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss']
})
export class BenefitsComponent implements OnInit {
  claimedPolicyList: any;
  activePolicyList: any;
  benefitColumns = Constants.BENEFITS_TABLE_COLUMNS;

  constructor(private commonService: CommonService,
    private router: Router) { }

  ngOnInit() {
    this.fetchBenefitsData();
  }

  fetchBenefitsData() {
    this.commonService.getPolicyData().subscribe((policyList) => {
      localStorage.setItem('items', JSON.stringify(policyList));
      this.claimedPolicyList = policyList;
      policyList.forEach(list => {
        list['insuredName'] = list['insured']['firstName'] + ' ' + list['insured']['lastName'];
      });
      this.activePolicyList = policyList.filter(function (list) {
        return list.policyClaimed === false;
      });
      this.claimedPolicyList = policyList.filter(function (list) {
        return list.policyClaimed === true;
      });
    }, (err) => {
      let list = localStorage.getItem('items');
      if (list) {
        let policyList = JSON.parse(list);
        this.claimedPolicyList = policyList;
        policyList.forEach(list => {
          list['insuredName'] = list['insured']['firstName'] + ' ' + list['insured']['lastName'];
        });
        this.activePolicyList = policyList.filter(function (list) {
          return list.policyClaimed === false;
        });
        this.claimedPolicyList = policyList.filter(function (list) {
          return list.policyClaimed === true;
        });
      }
    });
  }

  getDate(dateAndTime) {
    return dateAndTime.split('T', 1);
  }

  navigateToPolicyDetails(policyId) {
    const params: NavigationExtras = {
      queryParams: {
        policyId: policyId
      }
    };
    this.router.navigate(['/benefitDetails'], params);
  }

}
