import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CommonService } from '../../Common/Services/common.service';

@Component({
  selector: 'app-benefit-details',
  templateUrl: './benefit-details.component.html',
  styleUrls: ['./benefit-details.component.scss']
})
export class BenefitDetailsComponent implements OnInit {
  policyData: any;

  constructor(private route: ActivatedRoute, private commonService: CommonService, 
    private router: Router ) { }

  ngOnInit() {
    this.fetchBenefitDetails();
  }

  fetchBenefitDetails() {
    const param = this.route.snapshot.queryParams.policyId;
    this.commonService.getIndividualPolicyData(param).subscribe((policyData) => {
      this.policyData = policyData[0];
    }, (err) => {
      let policies = localStorage.getItem('items');
      if (policies) {
        let policy = (JSON.parse(policies) || []).find((i) => {
          return i.policyId === param;
        });
        this.policyData = policy;
      }
    });
  }

  getDate(dateAndTime) {
    return dateAndTime.split('T',1);
  }

  navigateToClaim() {
    const params: NavigationExtras = {
      queryParams: {
        policyId: this.route.snapshot.queryParams.policyId
      }
    };
    this.router.navigate(['/claims'], params);
  }

  goBacktoBenfits() {
    this.router.navigate(['/benefits']);
  }

}
