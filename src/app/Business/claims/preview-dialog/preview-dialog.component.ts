import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA, MatDialog, defaultRippleAnimationConfig } from '@angular/material';

@Component({
  selector: 'app-preview-dialog',
  templateUrl: './preview-dialog.component.html',
  styleUrls: ['./preview-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PreviewDialogComponent implements OnInit {
  previewObject = {
    'Gender': 'Male',
    'FullName': '',
    'Relationship': '',
    'dob': '1982-02-10',
    'attachDoc': '',
    'doa': '',
    'dos': '',
    'autoText': '',
    'servicePlace': '',
    'servicePerformed': '',
    'provider': ''
  };
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<PreviewDialogComponent>,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.generateForm();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  generateForm() {
    this.data.forEach(ele => {
      if (ele.questionLabel === 'Gender') {
        ele.options.forEach(innEle => {
          if (innEle.value) {
            this.previewObject.Gender = innEle.label
          }
        });
      } else if (ele.questionLabel === 'Patient Name ?') {
        let FullName = ''
        ele.options.forEach((innEle, index) => {
          FullName = FullName + ' ' + innEle.value
        });
        if (FullName) {
          this.previewObject.FullName = FullName
        } else {
          this.previewObject.FullName = 'NA'
        }
      } else if (ele.questionLabel === 'Relationship with Employee ?') {
        ele.options.forEach((innEle, index) => {
          if (innEle.value) {
            this.previewObject.Relationship = innEle.label
          }
        });
      } else if (ele.questionLabel === 'Date of Birth ?') {
        ele.options.forEach((innEle, index) => {
          if (innEle.value) {
            this.previewObject.dob = innEle.value
          }
        });
      } else if (ele.questionLabel === 'Upload Document ?') {
        ele.options.forEach((innEle, index) => {
          if (index === 0 && innEle.value) {
            this.previewObject.attachDoc = innEle.value[0][0].name;
          }
        });
      } else if (ele.questionLabel === 'Pay Provider ?') {
        ele.options.forEach((innEle, index) => {
          if (innEle.value) {
            this.previewObject.provider = innEle.label
          }
        });
      } else if (ele.isMutliple) {
        ele.questions.forEach(innerEle => {
          if (innerEle.questionLabel === 'Date of Accident ?') {
            innerEle.options.forEach((innEle, index) => {
              if (innEle.value) {
                this.previewObject.doa = innEle.value
              }
            });
          }
          if (innerEle.questionLabel === 'Date of Service ?') {
            innerEle.options.forEach((innEle) => {
              if (innEle.value) {
                this.previewObject.dos = innEle.value
              }
            });
          }
          if (innerEle.questionLabel === 'Service Performed ?') {
            innerEle.options.forEach((innEle) => {
              if (innEle.value) {
                this.previewObject.servicePerformed = innEle.value
              }
            });
          }
          if (innerEle.questionLabel === 'Place of Service ?') {
            innerEle.options.forEach((innEle) => {
              if (innEle.value) {
                this.previewObject.servicePlace = innEle.value
              }
            });
          }
        });
      }
    });
  }
}
