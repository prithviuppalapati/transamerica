import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Mock } from '../../Common/Mock/Mock';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PreviewDialogComponent } from './preview-dialog/preview-dialog.component';
import { CommonService } from './../../Common/Services/common.service';

declare var window: any;
declare var google: any;

@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClaimsComponent implements OnInit {
  questionSetsPatientInformation = [];
  questionSetsPolicyInformation = [];
  questionSet = [];
  queIndex = 0;
  progressiveCount = 5;
  isLast = false;
  isNextDiabled = true;
  attachments = [];
  photos = [];
  fname;
  lname;
  dob;
  doa;
  dos;
  accident;
  service;
  pAddress;
  sPerformed;
  relationship;

  @ViewChild('fileUpload', { static: false }) fileUpload: ElementRef;
  @ViewChild('photoUpload', { static: false }) photoUpload: ElementRef;



  constructor(private route: ActivatedRoute, private _snackBar: MatSnackBar,
    private router: Router, public dialog: MatDialog, private commonService: CommonService) { }

  ngOnInit() {
    this.questionSetsPatientInformation = JSON.parse(JSON.stringify(Mock.questionSetsPatientInformation));
    this.questionSetsPolicyInformation = JSON.parse(JSON.stringify(Mock.questionSetsPolicyInformation));
    this.questionSet = this.questionSet.concat(this.questionSetsPatientInformation, this.questionSetsPolicyInformation);
  }

  relationSelection(value) {
    if (value === 'Father') {
      this.fname = 'Robert'
      this.lname = 'Welch'
    } else if (value === 'Mother') {
      this.fname = 'Krista'
      this.lname = 'Patton' 
    }
  }

  selectOption(selectOption, options) {
    options.forEach(element => {
      element.value = false
    });
    selectOption.value = true;
    this.checkDisabled();
  }

  goBack() {
    const params: NavigationExtras = {
      queryParams: {
        policyId: this.route.snapshot.queryParams.policyId
      }
    };
    this.router.navigate(['/benefitDetails'], params);
  }

  goBacktoBenfits() {
    this.router.navigate(['/benefits']);
  }

  getPreview() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.width = '350px';
    dialogConfig.disableClose = true;
    dialogConfig.data = this.questionSet;
    dialogConfig.panelClass = 'full-width-dialog'
    const dialogRef = this.dialog.open(PreviewDialogComponent, dialogConfig);
  }

  checkDisabled() {
    const progCount = 100 / this.questionSet.length;
    this.questionSet.forEach((question, index) => {
      if (question.options) {
        question.options.forEach(data => {
          if (data.value) {
            this.progressiveCount = progCount * (index + 1);
          }
          if (data.value && index === this.queIndex) {
            this.isNextDiabled = false;
          }
        });
      } else {
        question.questions.forEach(innerQuestion => {
          innerQuestion.options.forEach(data => {
            if (data.value) {
              this.progressiveCount = progCount * (index + 1);
            }
            if (data.value && index === this.queIndex) {
              this.isNextDiabled = false;
            }
          });
        })
      }
    });
  }

  getNextQuestions() {
    this.queIndex++;
    this.isNextDiabled = true;
    this.checkDisabled();
    if ((this.questionSet.length - 1) == this.queIndex) {
      this.isLast = true;
    }
  }

  getPosition = function () {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    });
  }

  getAddress = latlng => {
    return new Promise((resolve, reject) => {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, (results, status) => {
            if (status === 'OK') {
                resolve(results[0]);
            } else {
                reject(status);
            }    
        });    
    });
};

  getLocation() {
    if (navigator.geolocation) {
      this.getPosition().then((position) => {
        var pos = {
          lat: position['coords'].latitude,
          lng: position['coords'].longitude
        };
        const latlng = new google.maps.LatLng(pos.lat, pos.lng);
        const geocoder = new google.maps.Geocoder();
        let _this = this;
        this.getAddress(pos).then((address) => {
          this.setMapLocationCallback.call(_this, address, status)
        })
      })
    }
  }

  setMapLocationCallback(a, b) {
    this.questionSet[this.queIndex].options[0].value = a.formatted_address;
    this.pAddress = a.formatted_address;
  }

  save() {

  }

  onSubmit() {
    if (!window.isDeviceOnline) {
      this._snackBar.open('No Internet Connection. We will Submit your claim, when your device is Online', 'Okay!', {
        duration: 4000,
      }).onAction().subscribe(() => {
        localStorage.setItem('offlineExists', 'yes');
        this.router.navigate(['']);
      });
    } else {
      const params: NavigationExtras = {
        queryParams: {
          policyId: this.route.snapshot.queryParams.policyId
        }
      };
      this.commonService.notifyUser({user: localStorage.getItem('name')}).subscribe((res) => {
        console.log(res);
    });
      this.router.navigate(['Confirmation'], params);
    }
  }

  getPreviousQuestions() {
    if (this.queIndex > 0) {
      this.queIndex--;
      this.isLast = false;
      this.isNextDiabled = false;
    }
  }

  onBrowseClick() {
    this.fileUpload.nativeElement.click();
  }

  onFileSelected(event, queIndex) {
    if (event.target.files && event.target.files.length > 0) {
      const files: File[] = event.target.files;
      this.attachments.push(files);
      this.questionSet[queIndex].options[0].value = this.attachments
    }
  }

  getPhoto() {
    this.photoUpload.nativeElement.click();
  }

  upload(queIndex): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
      const filePicker: any = document.querySelector('#photoUploadId');

      if (!filePicker || !filePicker.files
        || filePicker.files.length <= 0) {
        reject('No file selected.');
        return;
      }
      const myFile = filePicker.files[0];
      this.questionSet[queIndex].options[1].value = this.photos.push(myFile);
      resolve();
    });
  }

  deletePhoto(index) {
    this.photos.splice(index, 1);
  }

  deleteAttach(index) {
    this.attachments.splice(index, 1);
  }

}
