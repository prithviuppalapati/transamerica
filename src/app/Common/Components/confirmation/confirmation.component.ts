import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CommonService } from '../../Services/common.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationComponent implements OnInit {
  policyData: any;
  constructor(private route: ActivatedRoute, private commonService: CommonService, 
    private router: Router) { }

  ngOnInit() {
    this.fetchBenefitDetails();
  }

  fetchBenefitDetails() {
    const param = this.route.snapshot.queryParams.policyId;
    this.commonService.getIndividualPolicyData(param).subscribe((policyData) => {
      this.policyData = policyData[0];
    });
  }

  navigateTo() {
    this.router.navigate(['benefits']);
  }

  generatePDF() {
    const documentDefinition = {
      content:
      [
        {
          table:
          {
            headerRows: 1,
            widths: [ '*', '*', '*' ],
            body: [
              [
                { text: 'Confirmation Number', style: 'tableHeader' },
                { text: 'Policy Number', style: 'tableHeader' },
                { text: 'Policy Type', style: 'tableHeader' }
              ],
              [
                { text: '40768', style: 'tableHeader' },
                { text: this.policyData.policyId, style: 'tableHeader' },
                { text: this.policyData.policyType, style: 'tableHeader' }
              ]
            ]
          }
        },
        // tslint:disable-next-line: max-line-length
        'Your claim has been submitted. Please print and retain a copy of the claim form and authorisation of release of health information for your record. Please use the confirmation number provided above for inquiries related to request'
      ],
      styles:
      {
        header:
        {
          fontSize: 18,
          bold: true,
          margin: [0, 10, 0, 10],
          alignment: 'center'
        },
        tableHeader:
        {
          fillColor: '#fbffff',
          color: '#002c51',
          alignment: 'center',
          padding: '20px'
        }
      }
    };
    pdfMake.createPdf(documentDefinition).download('Receipt_' + this.policyData.policyId + '.pdf');
  }

}
