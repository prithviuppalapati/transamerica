import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  date: string;
  @Input() isHidden;
  constructor() { }
  ngOnInit() {
    this.date = new Date().toString().substring(4, 21).replace('2020', '2020,');
  }

  upload(): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
        const filePicker = document.querySelector('input');

        if (!filePicker || !filePicker.files 
            || filePicker.files.length <= 0) {
            reject('No file selected.');
            return;
        }
        const myFile = filePicker.files[0];
        console.log(myFile);

        resolve();
    });
  }
}
