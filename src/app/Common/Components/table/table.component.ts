import { Constants } from '../../Constants/constants';
import {
  Component, OnInit, Input, AfterViewInit,
  SimpleChanges, OnChanges, Output, EventEmitter
} from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ColumnType } from '../../enums/column-type';
import { TableColumn, Row } from '../../interfaces/table-column';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit, OnChanges {

  constructor(private datePipe: DatePipe) { }

  @Input() dataSet: Row[];
  @Input() policy: boolean;
  @Input() isStickyHeader: boolean;
  dataSource: MatTableDataSource<Row>;
  @Input() columns: TableColumn[];
  @Output() getDetails = new EventEmitter();
  columnTypes = ColumnType;


  ngOnInit() {
    this.dataSource = new MatTableDataSource<Row>(this.dataSet);
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource<Row>(this.dataSet);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.dataSet) {
      this.dataSource = new MatTableDataSource<Row>(changes.dataSet.currentValue);
    }
  }

  getDisplayColumnsList() {
    if (this.columns) {
      return this.columns.map(column => column.name);
    }
  }

  navigateToDetails(element) {
    this.getDetails.emit(element);
  }
}

