import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

  showFooter = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateTo(navigateToSection) {
    this.router.navigate([navigateToSection]);
  }

}
