import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from './../../Services/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder,private commonService: CommonService) { }
  loginForm: FormGroup;
  loginInformation;
  showAuthError: boolean;
  ngOnInit() {
    this.loginFormBuilder();
    this.loginInformation = {
      username : 'deloitte',
      password : 'deloitte@123'
    };
  }

  loginFormBuilder() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  navigateToBenefits() {
    this.showAuthError = false;
    localStorage.setItem('name', this.loginForm.value.username);
    this.commonService.loginUser({'user': this.loginForm.value.username}).subscribe((i) => {
      console.log(i);
    });
    if (this.loginForm.value.username.toUpperCase() === this.loginInformation.username.toUpperCase()
      && this.loginForm.value.password.toUpperCase() === this.loginInformation.password.toUpperCase()) {
      this.router.navigate(['/benefits']);
    } else {
      this.showAuthError = true;
      this.loginForm.reset();
    }
  }
}
