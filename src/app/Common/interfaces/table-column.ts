export interface TableColumn {
    displayName: string;
    name: string;
    type?: string;
    sort?: boolean;
    hyperLink?: boolean;
    dataType?: string;
}

export interface Row {
    [index: string]: number | string | Date | boolean;
}
