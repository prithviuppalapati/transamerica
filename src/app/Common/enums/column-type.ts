export enum ColumnType {
    NUMBER = 'number',
    STRING = 'string',
    CURRENCY = 'currency',
    DATE = 'date',
    DATE_TIME = 'date_time',
    TIME = 'time',
    MENU = 'menu'
}
