import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';  
import { environment } from '../../../environments/environment';

@Injectable()
export class CommonService {

  constructor(public http: HttpClient) { }

  getPolicyData(): Observable<any> {
    return this.http.get(`${environment.endpointIP}/getPolicyDetails`);
  }

  getIndividualPolicyData(policyId): Observable<any> {
    return this.http.get(`${environment.endpointIP}/getPolicyDetails/${policyId}`);
  }

  sendPushSubscription(body): Observable<any> {
    return this.http.post(`${environment.endpointIP}/subscription`, body);
  }

  loginUser(body): Observable<any> {
    return this.http.post(`${environment.endpointIP}/createuser`, body);
  }

  notifyUser(user):  Observable<any> {
    return this.http.post(`${environment.endpointIP}/notify`, user);
  }

}

