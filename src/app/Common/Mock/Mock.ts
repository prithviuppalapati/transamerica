export class Mock {
    public static questionSetsPatientInformation = [
        {
            'questionLabel': 'Relationship with Employee ?',
            'options': [
                {
                    'label': 'Self',
                    'value': false
                },
                {
                    'label': 'Father',
                    'value': false
                },
                {
                    'label': 'Mother',
                    'value': false
                },
                {
                    'label': 'Other',
                    'value': false
                }
            ],
            'type': 'radio'
        },
        {
            'questionLabel': 'Patient Name ?',
            'options': [
                {
                    'label': 'First Name',
                    'value': 'Robert'
                },
                {
                    'label': 'Last Name',
                    'value': 'Welch'
                }
            ],
            'type': 'input'
        },
        {
            'questionLabel': 'Providers Address ?',
            'options': [
                {
                    'label': 'Type here',
                    'value': ''
                }
            ],
            'type': 'textarea',
            'buttonText': 'Add Current Location'
        }
    ]
    public static questionSetsPolicyInformation = [
        {
            'questionLabel': 'Upload Document ?',
            'options': [
                {
                    'label': 'Attach Doc',
                    'value': ''
                },
                {
                    'label': 'Take Photo',
                    'value': ''
                }
            ],
            'type': 'button'
        },
        {
            questions: [
                {
                    'questionLabel': 'Date of Accident ?',
                    'options': [
                        {
                            'label': 'Select Date',
                            'value': ''
                        }
                    ],
                    'type': 'date'
                },
                {
                    'questionLabel': 'Date of Service ?',
                    'options': [
                        {
                            'label': 'Select Date',
                            'value': ''
                        }
                    ],
                    'type': 'date'
                }
            ],
            isMutliple: true
        },
        {
            questions: [
                {
                    'questionLabel': 'Place of Service ?',
                    'options': [
                        {
                            'label': 'Type here',
                            'value': ''
                        }
                    ],
                    'type': 'textarea'
                },
                {
                    'questionLabel': 'Service Performed ?',
                    'options': [
                        {
                            'label': 'Type here',
                            'value': ''
                        }
                    ],
                    'type': 'textarea'
                }
            ],
            isMutliple: true
        },
        {
            'questionLabel': 'Pay Provider ?',
            'options': [
                {
                    'label': 'Pay benefit to provider',
                    'value': false
                },
                {
                    'label': 'Pay benefit to me',
                    'value': false
                }
            ],
            'type': 'radio'
        }
    ]
    public static uploadDocument = [

    ]
}