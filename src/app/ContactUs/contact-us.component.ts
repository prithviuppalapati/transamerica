import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    console.log('Contact us loaded');
  }

  onSubmit(formData: NgForm) {
    console.log('formSubmitted:', formData);
  }
}
