module.exports = {
  "globDirectory": "dist/pwa-starter",
  "globPatterns": [
    "**/*.{html,css,ico,js,png,jpg,svg}"
  ],
  "swDest": "dist/pwa-starter/sw.js",
  "swSrc": "src/sw.js",
  "maximumFileSizeToCacheInBytes": 5000000
};