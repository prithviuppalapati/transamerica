var express = require('express');
var path = require('path');
var app = express();
var router = express.Router();
var bodyParser = require('body-parser')
var compression = require('compression');
const webpush = require("web-push");
const benefits = require("./app/controller/policy.js");
var mongoose = require('mongoose');
var User = require("./app/models/profile");
var httpsRedirect = require("express-https-redirect");

webpush.setGCMAPIKey("AIzaSyBboKguD_HSDn4JXw0d77AyZwX-FYpuNhI");
const options = {
  gcmAPIKey:
    "AIzaSyBboKguD_HSDn4JXw0d77AyZwX-FYpuNhI",
  vapidDetails: {
    subject: "mailto:prituppalapati@gmail.com",
    publicKey:
      "BD15-ky6pTNm3dDaWU9hpTY2Y02C7NTy5VH79EuRI7vhStTKODXezFm06qRVOwHUfZm7qmEljOKwlnoA18RM_ZI",
    privateKey: "BmFwOWre41qKNNmWOOXWm8Heu9NMQJyJj9_KYS6J0pw"
  },
  TTL: 10,
  headers: {}
};


mongoose.connect('mongodb://prithvi:prithvi1369@ds127634.mlab.com:27634/pwa');
app.use(compression());
app.use("/", httpsRedirect());
app.use(express.static(path.join(__dirname, './dist/pwa-starter')));

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())


/// app runs in port
app.listen(process.env.PORT || 3000, function () {
  console.log('listening at 3000');
})

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "POST, GET, OPTIONS, DELETE, PUT"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Retrieve all policies
app.get("/getPolicyDetails", benefits.findAll);

// Retrieve particular policy
app.get("/getPolicyDetails/:policyId", benefits.findOne);

//get device subscription
app.post("/subscription", function (req, res) {

  User.findOne({ 'name': req.body.user, "subscription.endpoint": req.body.endpoint }, function (err, doc) {
    if (err) {
      console.log(err);
    }
    if (!doc) {
      User.findOneAndUpdate({ 'name': req.body.user || 'test' }, { $push: { subscription: { endpoint: req.body.endpoint, p256dh: req.body.keys.p256dh, auth: req.body.keys.auth } } }, { new: true }).exec(
        function (err, doc, updated) {
          if (err) {
            console.log(err);
          }
        }
      );
    } else {
      var obj = JSON.parse(JSON.stringify(doc));
      if (obj.subscription && obj.subscription.length == 0) {
        User.findOneAndUpdate({ 'name': req.body.user || 'test' }, { $push: { subscription: { endpoint: req.body.endpoint, p256dh: req.body.keys.p256dh, auth: req.body.keys.auth } } }, { new: true }).exec(
          function (err, doc) {
            if (err) {
              console.log(err);
            }
          }
        );
      }
    }
  });
  res.send({});
});

// create user

app.post("/createuser", function (req, res) {

  User.findOne({ 'name': req.body.user }, function (err, doc) {
    if (err) {
      console.log(err);
    }
    if (!doc) {
      var user = new User({
        name: req.body.user
      });
      user.save(function (err, saved) {
        if (err) {
          console.log(err);
        }
      });
    }
  });
  res.send({});
});

app.post("/notify", function (req, res) {
  User.findOne({ 'name': req.body.user }, function (err, doc) {
    if (doc) {
      const subscription = JSON.parse(JSON.stringify(doc)).subscription || [];
      var obj = {
        title:`${req.body.user}, your claim has been submitted`,
      };
      subscription.forEach(sub => {
        console.log(sub);
        var pushSubscription = { endpoint: sub.endpoint, keys: { auth: sub.auth, p256dh: sub.p256dh } };
        webpush
          .sendNotification(pushSubscription, JSON.stringify(obj), options)
          .then(function (err) {
            console.log(err);
          });
      });
    }
  })
  res.send({});
});


app.get("/notifyUser", function (req, res) {
  var name = req.query.name || 'prithvi';
  User.findOne({ name  }, function (err, doc) {
    if (doc) {
      const subscription = JSON.parse(JSON.stringify(doc)).subscription || [];
      var obj = {
        title:`${name}, Your claim has been submitted`,
      };
      subscription.forEach(sub => {
        var pushSubscription = { endpoint: sub.endpoint, keys: { auth: sub.auth, p256dh: sub.p256dh } };
        webpush
          .sendNotification(pushSubscription, JSON.stringify(obj), options)
          .then(function (err) {
            console.log(err);
          });
      });
    }
  })
  res.send({});
});

app.get("/notifyAll", function (req, res) {
  User.find({}, function (err, docs) {
    let pDocs = JSON.parse(JSON.stringify(docs));
    pDocs.forEach((doc) => {
      if (doc) {
        const subscription = JSON.parse(JSON.stringify(doc)).subscription || [];
        var obj = {
          title:`${doc.name}, Your claim has been submitted`,
        };
        subscription.forEach(sub => {
          console.log(sub);
          var pushSubscription = { endpoint: sub.endpoint, keys: { auth: sub.auth, p256dh: sub.p256dh } };
          webpush
            .sendNotification(pushSubscription, JSON.stringify(obj), options)
            .then(function (err) {
              console.log(err);
            });
        });
      }
    })
  })
  res.send({});
});