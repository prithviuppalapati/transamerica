
const policyList = require('../mockdata.js');

// Retrieve and return all policies from mock file
exports.findAll = (req, res) => {
    res.send(policyList);
};

// Retrieve and return one matched policy from mock file
exports.findOne = (req, res) => {
    const response = policyList.find(each => each.policyId === req.params.policyId);
    res.send(response ? [response] : []);
};

