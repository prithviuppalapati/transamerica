var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserSchema = new Schema({
  name: String,
  subscription: [{ endpoint: String, p256dh: String, auth: String }]
});
module.exports = mongoose.model("User", UserSchema);
