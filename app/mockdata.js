module.exports = [
    {
        policyId: "G0926TS53",
        policyType: "Critical Illness",
        policyStatus: "Active",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 50769,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS54",
        policyType: "Universal Life",
        policyStatus: "Active",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40581,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS55",
        policyType: "Accident",
        policyStatus: "Active",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40689,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS56",
        policyType: "TranceConnect",
        policyStatus: "Active",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40207,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS43",
        policyType: "Critical Illness",
        policyStatus: "Claimed",
        policyExpiryDate: "2019-11-22T18:30:00.000Z",
        policyClaimed: true,
        policyTerm: "yearly",
        policyStartDate: "2018-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40768,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS44",
        policyType: "Universal Life",
        policyStatus: "Active",
        policyExpiryDate: "2019-11-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2018-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40900,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS45",
        policyType: "Accident",
        policyStatus: "Claimed",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: true,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40769,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0926TS46",
        policyType: "TranceConnect",
        policyStatus: "Claimed",
        policyExpiryDate: "2020-11-22T18:30:00.000Z",
        policyClaimed: true,
        policyTerm: "yearly",
        policyStartDate: "2019-11-22T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40770,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    },
    {
        policyId: "G0925TS55",
        policyType: "Accident",
        policyStatus: "About Lapse",
        policyExpiryDate: "2020-03-22T18:30:00.000Z",
        policyClaimed: false,
        policyTerm: "yearly",
        policyStartDate: "2019-11-23T18:30:00.000Z",
        owner: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        insured: {
            firstName: "Franklin",
            lastName: "Clara",
            dob: "1990-07-25T18:30:00.000Z",
            email: "franklin.clara@testmail.com",
            phone: "(614) 724 5386",
            mobile: "(614) 724 5386"
        },
        address: [{
            type: "home",
            addressLine1: "518 E Broadway street",
            addtressLine2: "",
            city: "columbus",
            state: "ohio",
            country: "US",
            zipcode: 500089
        }],
        premiumDetails: {
            amount: 602,
            frequency: "monthly",
            method: "Payroll deducted",
            unit: "$"
        },
        claimDetails: {
            confirmationNo: 40689,
            claimDate: "2019-04-23T18:30:00.000Z"
        }
    }
];
